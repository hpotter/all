package com.example.project.a5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu; import android.view.MenuItem;
import android.widget.Button; import android.widget.TextView;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.ObjectOutputStream;
import java.io.FileReader; import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.valueOf;
public class MainActivity extends AppCompatActivity {
    public TextView textView;
    double calc;
    double temp;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        calc=0;
        textView = (TextView) findViewById(R.id.textView);
    }
    ArrayList<String> arrayList =new ArrayList<String>();
    String string= "";
    String string1= "";
    //this function accepts the equation and sorts it
    public void onClick1 (View v)
    {
        TextView textView2= (TextView) findViewById(R.id.textView2);
        Button button = (Button) v;
        string= (String) button.getText().toString();

        if ( !string.contains("+") && !string.contains("-") && !string.contains("*") && !string.contains("/"))
        {
            string1= string1+ string;
            if(arrayList.size()>0)
            {
                arrayList.remove(arrayList.size()-1);
            }
            arrayList.add(string1);
            calc = Double.parseDouble(string1);
        }
        else
        {
            arrayList.add(string);
            arrayList.add(string);
            string1="";

        }



        textView2.setText(textView2.getText().toString()+ string);

        //textView2.setText(string);
    }
    //this function performs the actual calculation
    public void onClick (View v) {
        Double calc=0.0;
        int c= arrayList.size();
        while(c!=1)
        {
            if (c>3)
            {
                if(arrayList.get(3).contains("*") ||arrayList.get(3).contains("/"))
                {
                    if(arrayList.get(3).contains("*"))  { calc= Double.parseDouble(arrayList.get(2)) * Double.parseDouble(arrayList.get(4));}
                    if(arrayList.get(3).contains("/"))  { calc= Double.parseDouble(arrayList.get(2))/ Double.parseDouble(arrayList.get(4));}
                    arrayList.remove(2);
                    arrayList.remove(2);
                    arrayList.remove(2);
                    arrayList.add(2, Double.toString(calc));
                    c=arrayList.size();
                }
                else
                {
                    if (arrayList.get(1).contains("+")) {calc= Double.parseDouble(arrayList.get(0)) +Double.parseDouble(arrayList.get(2));}
                    if (arrayList.get(1).contains("-")) {calc= Double.parseDouble(arrayList.get(0)) - Double.parseDouble(arrayList.get(2));}
                    if (arrayList.get(1).contains("*")) {calc= Double.parseDouble(arrayList.get(0)) * Double.parseDouble(arrayList.get(2));}
                    if (arrayList.get(1).contains("/")) {calc= Double.parseDouble(arrayList.get(0)) / Double.parseDouble(arrayList.get(2));}
                    arrayList.remove(0);
                    arrayList.remove(0);
                    arrayList.remove(0);
                    arrayList.add(0, Double.toString(calc));
                    c=arrayList.size();
                }
            }
            else
            {
                if (arrayList.get(1).contains("+")) {
                    calc= Double.parseDouble(arrayList.get(0)) + Double.parseDouble(arrayList.get(2));
                }
                if (arrayList.get(1).contains("-")) {
                    calc= Double.parseDouble(arrayList.get(0)) - Double.parseDouble(arrayList.get(2));
                }
                if (arrayList.get(1).contains("*")) {
                    calc= Double.parseDouble(arrayList.get(0)) * Double.parseDouble(arrayList.get(2));
                }
                if (arrayList.get(1).contains("/")){
                    calc= Double.parseDouble(arrayList.get(0)) / Double.parseDouble(arrayList.get(2));
                }
                arrayList.remove(0);
                arrayList.remove(0);
                arrayList.remove(0);
                arrayList.add(0, Double.toString(calc));
                c=arrayList.size();
            }
        }
        if(arrayList.size()==1) {
            calc= Double.parseDouble(arrayList.get(0));
        }
        textView.setText("" + calc);
    }
    public void sinFunct(View v) {
        textView.setText("" + Math.sin(calc));
    }
    public void cosecfn(View v) {
        textView.setText("" + 1/(Math.sin(calc)));
    }
    public void secfn(View v) {
        textView.setText("" + 1/(Math.cos(calc)));
    }
    public void cosfn(View v) {
        textView.setText("" + Math.cos(calc));
    }
    public void tanfn(View v) {
        textView.setText("" + Math.tan(calc));
    }
    public void cotfn(View v) {
        textView.setText("" + 1/Math.tan(calc));
    }
    public void sqrtfn(View v) {
        textView.setText("" + Math.sqrt(calc));
    }
    public void storeres(View v)
    {
        try
        {
            OutputStreamWriter out = new OutputStreamWriter(openFileOutput("try.txt",0));
            out.write(""+calc);
            out.close();
            //FileWriter f = new FileWriter("result.txt");
            //f.write("" + calc);
            //textView.setText("");
            //FileReader r= new FileReader("result.txt");
            //f.close();
        }
        catch(Exception e) { }
    }
    public void ansres(View v)
    {
        onClick(v);
        TextView textView= (TextView) findViewById(R.id.textView);
        TextView textView2= (TextView) findViewById(R.id.textView2);
        textView2.setText(textView.getText().toString());
    }

    public void clear(View v)
    {
        TextView textView = (TextView) findViewById(R.id.textView);
        TextView textView2 =(TextView) findViewById(R.id.textView2);
        string1="";
        string="";
        textView.setText("0");
        textView2.setText("");
        arrayList.clear();
    }
}
